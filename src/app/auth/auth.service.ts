import { Injectable } from '@angular/core';

const TOKEN_KEY = 'id_token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(token: string): void {
    localStorage.setItem(TOKEN_KEY, token);
  }
  
  isLoggedIn(): boolean {
    const token = localStorage.getItem(TOKEN_KEY);
    return !!token;
  }
  
  logout(): void {
    localStorage.removeItem(TOKEN_KEY);
  }
}