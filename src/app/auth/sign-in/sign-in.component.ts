import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from '../auth.service';

const COGNITO_URL = "https://veratrak-angular-training.auth.eu-west-1.amazoncognito.com/login?client_id=2dmik0k7i03jl90v87ritrvn6s&response_type=code&scope=email+openid&redirect_uri=http://localhost:4200/";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params: Params) => {
      if (params.code) {
        this.authService.login(params.code);
      }
    });
    
    if (this.authService.isLoggedIn()) {
      this.router.navigate(["dashboard"]);
    } else {
      window.location.href = COGNITO_URL;
    }
  }
}